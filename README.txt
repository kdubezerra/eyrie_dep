**eyrie** allows you to implement a state-machine replicated service with partitioned state easily,
by just making small extensions to some classes of Eyrie

**eyrie_dep** is just a parent repository to the following modules:
 - libmcad (multicast adaptor interface that allows eyrie to be used with different atomic multicast implementations)
 - Ridge (high-throughput, low-latency atomic multicast with optimistic deliveries)
 - Multi-Ring-Paxos (high-throughput atomic multicast that uses ring topologies)
 - sense (performance monitoring library)
 - netwrapper (simple api for TCP/IP using java; it wraps a multi-threaded network implementation with Selectors)


The fastest way to test S-SMR (Scalable State-Machine Replication, implemented by Eyrie) is:

1) git clone --recursive https://bitbucket.org/kdubezerra/eyrie_dep.git
2) cd eyrie_dep
3) mvn package -DskipTests -Dmaven.javadoc.skip=true
4) cd exampleLink

5.1) you can run all processes and launch the example application with:
     ./deployerFull.py
     
5.2) alternatively, you can run each component of the system in a different screen:
     at a terminal 1, run ./deployMcast.py
     at a terminal 2, run ./deployServer_i.py 9 1
     at a terminal 3, run ./deployServer_i.py 10 2
     at a terminal 4, run ./deployClient_i.py 123
